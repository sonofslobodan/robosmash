﻿using UnityEngine;

public class PlayerInfo
{
    private const string HighScoreKey = "PlayerHighScore";

    public static int HighScore => PlayerPrefs.HasKey(HighScoreKey) ? PlayerPrefs.GetInt(HighScoreKey) : 0;

    public static void SaveHighScore(int score) => PlayerPrefs.SetInt(HighScoreKey, score);

    public static DifficultySettings DifficultySettings { get; private set; }

    public static void Initialize(DifficultySettings difficultySettings) => DifficultySettings =
        new DifficultySettings(
            difficultySettings.LeftSpawnerSpawnRate, 
            difficultySettings.RightSpawnerSpawnRate,
            difficultySettings.TopSpawnerSpawnRate, 
            difficultySettings.EnemyMoveSpeedMultiplier,
            difficultySettings.EnemyDamageMultiplier,
            difficultySettings.ScoreMultiplier);
}