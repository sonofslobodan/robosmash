﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

public class Shield : MonoBehaviour
{
    [SerializeField, Range(0, 100f)] private float maxShield = 50f;
    [SerializeField] private float shieldRechargeRate = 5f;
    [SerializeField] private float shieldDecayRate = 10f;
    [SerializeField] private Material[] outlinedMaterials = null;
    [SerializeField] private float maxOutlineWidth = 0;
    [SerializeField] private Color outlineColor = Color.blue;
    [SerializeField] private RectTransform energyBar = null;

    private readonly int outlineThicknessPropertyId = Shader.PropertyToID("_Outline");
    private readonly int outlineColorPropertyId = Shader.PropertyToID("_OutlineColor");

    private const float ShieldInterpolationTime = 0.5f;
    private const float InterpolationStep = 1 / ShieldInterpolationTime;
    private const float EnergyBarInterpolationSpeed = 15f;

    private float energyBarStep;
    private float currentInterpolationTime;

    private bool shieldActivated;

    public bool ShieldActivated
    {
        get { return shieldActivated; }
        set
        {
            if (shieldActivated == value)
            {
                return;
            }

            shieldActivated = value;
            currentInterpolationTime = 0;
        }
    }

    public float CurrentShield { get; private set; }

    private void Awake()
    {
        Assert.IsTrue(outlinedMaterials.Length > 0);
        Assert.IsNotNull(energyBar);

        energyBarStep = 1 / maxShield;

        CurrentShield = maxShield;
        foreach (Material m in outlinedMaterials)
        {
            m.SetColor(outlineColorPropertyId, outlineColor);
        }
    }

    private void OnDestroy()
    {
        foreach (Material m in outlinedMaterials)
        {
            m.SetFloat(outlineThicknessPropertyId, 0);
        }
    }

    private void Update()
    {
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (!(Math.Abs(CurrentShield) == 0 && InputControls.Shield))
        {
            CurrentShield = ShieldActivated
                ? Mathf.Max(CurrentShield - Time.deltaTime * shieldDecayRate, 0)
                : Mathf.Min(CurrentShield + Time.deltaTime * shieldRechargeRate, maxShield);
        }

        InterpolateEnergyBar();

        if (currentInterpolationTime > ShieldInterpolationTime)
        {
            return;
        }

        foreach (Material m in outlinedMaterials)
        {
            m.SetFloat(outlineThicknessPropertyId,
                Mathf.Lerp(m.GetFloat(outlineThicknessPropertyId), shieldActivated ? maxOutlineWidth : 0,
                    SmoothLerp.SmoothStep(currentInterpolationTime * InterpolationStep)));
        }

        currentInterpolationTime += Time.deltaTime;
    }

    private void InterpolateEnergyBar() => energyBar.transform.localScale = 
        Vector2.Lerp(energyBar.transform.localScale,
        new Vector2(CurrentShield * energyBarStep, energyBar.transform.localScale.y),
        Time.deltaTime * EnergyBarInterpolationSpeed);
}