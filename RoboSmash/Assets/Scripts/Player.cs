﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private float maxHealth = 100;
    [SerializeField] private HitBox fistHitBox = null;
    [SerializeField] private HitBox bodyHitBox = null;
    [SerializeField] private RectTransform healthBar = null;
    [SerializeField] private Text scoreText = null;
    [SerializeField] private RoboController roboController = null;
    [SerializeField] private GameObject gameOverPanel = null;

    private float healthStep;

    private float currentHealth;

    public float CurrentHealth
    {
        get { return currentHealth; }
        private set
        {
            if (Math.Abs(CurrentHealth - value) < 0.001f)
            {
                return;
            }

            currentHealth = value;
            enabled = true;
        }
    }

    private int currentScore;

    public int CurrentScore
    {
        get { return currentScore; }
        set
        {
            if (CurrentScore == value)
            {
                return;
            }

            currentScore = value;
            scoreText.text = $"Score: {currentScore}";
        }
    }

    private const float HealthBarInterpolationSpeed = 15f;

    public const int RoboLayer = 10;

    private void Awake()
    {
        Assert.IsNotNull(fistHitBox);
        Assert.IsNotNull(bodyHitBox);
        Assert.IsNotNull(healthBar);
        Assert.IsNotNull(scoreText);
        Assert.IsNotNull(roboController);
        Assert.IsNotNull(gameOverPanel);

        currentHealth = maxHealth;
        healthStep = 1 / maxHealth;

        bodyHitBox.TriggeredEvent += OnBodyTriggered;
    }

    private void OnDestroy()
    {
        bodyHitBox.TriggeredEvent -= OnBodyTriggered;
    }

    private void OnBodyTriggered(Collider other)
    {
        if (other.gameObject.layer != Enemy.EnemyLayer)
        {
            return;
        }

        if (roboController.Shield.ShieldActivated)
        {
            bodyHitBox.CurrentHitForce = fistHitBox.CurrentHitForce;
        }
        else
        {
            bodyHitBox.ResetHitForce();

            CurrentHealth -= other.GetComponent<Enemy>().Damage;
            if (CurrentHealth < 1)
            {
                GameOver();
            }
        }
    }

    private void GameOver()
    {
        enabled = false;
        bodyHitBox.enabled = false;
        fistHitBox.enabled = false;
        roboController.enabled = false;
        healthBar.transform.localScale = Vector3.zero;
        roboController.Shield.ShieldActivated = false;

        if (CurrentScore > PlayerInfo.HighScore)
        {
            PlayerInfo.SaveHighScore(CurrentScore);
        }

        gameOverPanel.SetActive(true);
    }

    private void Update()
    {
        if (Math.Abs(currentHealth * healthStep - healthBar.transform.localScale.x) < 0.01f)
        {
            enabled = false;
        }

        healthBar.transform.localScale = Vector2.Lerp(healthBar.transform.localScale,
            new Vector2(currentHealth * healthStep, healthBar.transform.localScale.y),
            Time.deltaTime * HealthBarInterpolationSpeed);
    }
}