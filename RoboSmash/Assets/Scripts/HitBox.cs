﻿using System;
using UnityEngine;

public class HitBox : MonoBehaviour
{
    [SerializeField] private float hitForce = 500;

    public event Action<Collider> TriggeredEvent = collider => { };

    private float? currentHitForce;
    public float CurrentHitForce
    {
        get { return currentHitForce ?? hitForce; }
        set { currentHitForce = value; }
    }

    private void OnTriggerEnter(Collider other)
    {
        TriggeredEvent(other);
        Vector3 normalizedDirection = (other.transform.position - transform.position).normalized;
        other.attachedRigidbody.AddForce(new Vector3(normalizedDirection.x, normalizedDirection.y, 0) * CurrentHitForce);
    }

    public void ResetHitForce() => CurrentHitForce = hitForce;
}