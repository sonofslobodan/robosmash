﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    [SerializeField] private Text continueText = null;

    private void Awake()
    {
        Assert.IsNotNull(continueText);

        continueText.text = $"Press {InputControls.ContinueKey} to continue.";
    }

    private void Update()
    {
        if (InputControls.Continue)
        {
            SceneManager.LoadScene(MainMenu.MainMenuScene);
        }
    }
}
