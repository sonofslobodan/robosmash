﻿using UnityEngine;

public static class InputControls
{
    public const string MoveLeftKey = "a";
    public const string MoveRightKey = "d";
    public const string JumpKey = "space";
    public const string ContinueKey = "c";
    public const int AttackMouseButton = 0;
    public const int ShieldMouseButton = 1;

    public static bool MoveLeft => Input.GetKey(MoveLeftKey);
    public static bool MoveRight => Input.GetKey(MoveRightKey);
    public static bool Jump => Input.GetKeyDown(JumpKey);
    public static bool Attack => Input.GetMouseButtonDown(AttackMouseButton);
    public static bool Shield => Input.GetMouseButton(ShieldMouseButton);
    public static bool Continue => Input.GetKeyDown(ContinueKey);
}
