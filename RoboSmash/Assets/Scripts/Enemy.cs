﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Rigidbody enemyRigidbody = null;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float torqueMultiplier = 30f;
    [SerializeField] private float damage = 3;
    [SerializeField] private int scoreValue = 3;

    private readonly WaitForSeconds disableAfterHitTime = new WaitForSeconds(2f);

    private Coroutine disableProcess;

    public Vector3 TargetDirection { get; set; }

    public float Damage => PlayerInfo.DifficultySettings == null
        ? damage
        : damage * PlayerInfo.DifficultySettings.EnemyDamageMultiplier;

    private float MoveSpeed => PlayerInfo.DifficultySettings == null
        ? moveSpeed
        : moveSpeed * PlayerInfo.DifficultySettings.EnemyMoveSpeedMultiplier;

    private int ScoreValue => PlayerInfo.DifficultySettings == null
        ? scoreValue
        : Mathf.CeilToInt(scoreValue * PlayerInfo.DifficultySettings.ScoreMultiplier);

    private bool hitByRobo;

    public bool HitByRobo
    {
        get { return hitByRobo; }
        set
        {
            if (HitByRobo == value)
            {
                return;
            }

            hitByRobo = value;

            if (hitByRobo)
            {
                enemySpawner.Player.CurrentScore += ScoreValue;
            }
        }
    }

    public const int EnemyLayer = 12;

    private EnemySpawner enemySpawner;

    private void Awake()
    {
        Assert.IsNotNull(enemyRigidbody);
    }

    private void OnEnable()
    {
        HitByRobo = false;
        enemyRigidbody.isKinematic = true;
        enemyRigidbody.isKinematic = false;
        enemyRigidbody.AddTorque(Vector3.up * torqueMultiplier);
    }

    public void Initialize(EnemySpawner enemySpawner) => this.enemySpawner = enemySpawner;

    // ReSharper disable once UnusedParameter.Local
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == Player.RoboLayer)
        {
            HitByRobo = true;
        }

        StartDelayedDisable();
    }

    // ReSharper disable once UnusedParameter.Local
    private void OnCollisionEnter(Collision other)
    {
        if (HitByRobo && other.gameObject.layer == EnemyLayer)
        {
            other.transform.GetComponent<Enemy>().HitByRobo = true;
        }

        StartDelayedDisable();
    }

    private void StartDelayedDisable()
    {
        enemyRigidbody.useGravity = true;

        if (disableProcess == null)
        {
            disableProcess = StartCoroutine(DelayedDisable());
        }
    }

    private IEnumerator DelayedDisable()
    {
        yield return disableAfterHitTime;
        enemyRigidbody.useGravity = false;
        gameObject.SetActive(false);
        disableProcess = null;
    }

    private void Update()
    {
        if (disableProcess == null)
        {
            enemyRigidbody.MovePosition(transform.position + TargetDirection * MoveSpeed);
        }
    }
}