﻿using System;
using UnityEngine;

[Serializable]
public class DifficultySettings
{
    [SerializeField, Range(0.1f, 5f)] private float leftSpawnerSpawnRate;
    [SerializeField, Range(0.1f, 5f)] private float rightSpawnerSpawnRate;
    [SerializeField, Range(5f, 20f)] private float topSpawnerSpawnRate;
    [SerializeField, Range(0.1f, 5f)] private float enemyMoveSpeedMultiplier;
    [SerializeField, Range(0.1f, 5f)] private float enemyDamageMultiplier;
    [SerializeField, Range(0.1f, 5f)] private float scoreMultiplier;

    public float LeftSpawnerSpawnRate => leftSpawnerSpawnRate;
    public float RightSpawnerSpawnRate => rightSpawnerSpawnRate;
    public float TopSpawnerSpawnRate => topSpawnerSpawnRate;
    public float EnemyMoveSpeedMultiplier => enemyMoveSpeedMultiplier;
    public float EnemyDamageMultiplier => enemyDamageMultiplier;
    public float ScoreMultiplier => scoreMultiplier;

    public DifficultySettings(float leftSpawnerSpawnRate, float rightSpawnerSpawnRate, float topSpawnerSpawnRate,
        float enemyMoveSpeedMultiplier, float enemyDamageMultiplier, float scoreMultiplier)
    {
        this.leftSpawnerSpawnRate = leftSpawnerSpawnRate;
        this.rightSpawnerSpawnRate = rightSpawnerSpawnRate;
        this.topSpawnerSpawnRate = topSpawnerSpawnRate;
        this.enemyMoveSpeedMultiplier = enemyMoveSpeedMultiplier;
        this.enemyDamageMultiplier = enemyDamageMultiplier;
        this.scoreMultiplier = scoreMultiplier;
    }
}