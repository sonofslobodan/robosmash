﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

public class RoboController : MonoBehaviour
{
    [SerializeField] private Animator roboAnimator = null;
    [SerializeField] private Shield shield = null;
    [SerializeField] private float moveSpeed = 1f;
    [SerializeField] private float airMoveSpeed = 0.5f;
    [SerializeField] private float jumpHeight = 2f;
    [SerializeField] private float jumpDuration = 1f;
    [SerializeField] private float rotationSpeed = 10f;

    private readonly int stateHash = Animator.StringToHash("State");

    private readonly WaitForSeconds jumpDelay = new WaitForSeconds(0.25f);

    private Vector3 startForward;
    private Vector3 moveDirection;
    private Vector3 facingDirection;

    private Coroutine jumpProcess;

    private float halfDuration;
    private float interpolationStep;
    private float currentJumpTime;

    private bool airBorne;

    private const float ScreenBoundsOffset = 0.05f;

    private enum AnimationState
    {
        Idle,
        Walking,
        Jumping,
        Attacking
    }

    private AnimationState currentAnimationState;

    public Shield Shield => shield;

    private void Awake()
    {
        Assert.IsNotNull(roboAnimator);
        Assert.IsNotNull(shield);

        halfDuration = jumpDuration / 2;
        interpolationStep = 1 / halfDuration;

        startForward = transform.forward;
        facingDirection = startForward;
    }

    private void OnDisable() =>
        roboAnimator.SetInteger(stateHash, (int) (currentAnimationState = AnimationState.Idle));

    private void Update()
    {
        PollInput();
        FaceDirection();
    }

    // ReSharper disable once UnusedMember.Global
    // Used as animation event for battle idle
    public void SetIdle() => roboAnimator.SetInteger(stateHash, (int) (currentAnimationState = AnimationState.Idle));

    private void PollInput()
    {
        shield.ShieldActivated = InputControls.Shield && shield.CurrentShield > 0;

        if (InputControls.MoveLeft || InputControls.MoveRight)
        {
            if (currentAnimationState != AnimationState.Jumping && currentAnimationState != AnimationState.Attacking)
            {
                roboAnimator.SetInteger(stateHash, (int) (currentAnimationState = AnimationState.Walking));
            }

            MoveHorizontally();
        }
        else if (currentAnimationState == AnimationState.Walking)
        {
            SetIdle();
        }

        if (InputControls.Jump && currentAnimationState != AnimationState.Attacking &&
            currentAnimationState != AnimationState.Jumping && jumpProcess == null)
        {
            StartJump();
        }

        if (InputControls.Attack)
        {
            roboAnimator.SetInteger(stateHash, (int) (currentAnimationState = AnimationState.Attacking));
        }
    }

    private void MoveHorizontally()
    {
        if (currentAnimationState == AnimationState.Attacking && !airBorne)
        {
            return;
        }

        moveDirection = InputControls.MoveRight ? startForward : -startForward;

        if (!airBorne)
        {
            facingDirection = InputControls.MoveRight ? startForward : -startForward;
        }

        float xBoundsPos = Camera.main.WorldToViewportPoint(transform.position).x;

        if (InputControls.MoveRight && xBoundsPos > 1 - ScreenBoundsOffset ||
            InputControls.MoveLeft && xBoundsPos < ScreenBoundsOffset)
        {
            return;
        }

        transform.position += moveDirection *
                              (currentAnimationState == AnimationState.Jumping ? airMoveSpeed : moveSpeed) *
                              Time.deltaTime;
    }

    private void FaceDirection() => transform.rotation = Quaternion.Slerp(transform.rotation,
        Quaternion.LookRotation(facingDirection), Time.deltaTime * rotationSpeed);

    private void StartJump()
    {
        airBorne = true;
        jumpProcess = StartCoroutine(Jump());
        roboAnimator.SetInteger(stateHash, (int) (currentAnimationState = AnimationState.Jumping));
    }

    private IEnumerator Jump()
    {
        yield return jumpDelay;

        float startYPos = transform.position.y;
        float endYPos = transform.position.y + jumpHeight;

        currentJumpTime = 0;
        // Ascend
        while (currentJumpTime < halfDuration)
        {
            InterpolateYPos(startYPos, endYPos, true);
            yield return null;
        }

        currentJumpTime = 0;
        // Descend
        while (currentJumpTime < halfDuration)
        {
            InterpolateYPos(endYPos, startYPos, false);
            yield return null;
        }

        transform.position = new Vector3(transform.position.x, startYPos, transform.position.z);

        airBorne = false;
        jumpProcess = null;
    }

    private void InterpolateYPos(float startYPos, float endYPos, bool ascend)
    {
        transform.position = Vector3.Lerp(
            new Vector3(transform.position.x, startYPos, transform.position.z),
            new Vector3(transform.position.x, endYPos, transform.position.z),
            ascend
                ? SmoothLerp.Sinerp(currentJumpTime * interpolationStep)
                : SmoothLerp.Coserp(currentJumpTime * interpolationStep));

        currentJumpTime += Time.deltaTime;
    }
}