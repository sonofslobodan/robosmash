﻿using UnityEngine;

public static class SmoothLerp
{
    public static float Sinerp(float t) => Mathf.Sin(t * Mathf.PI * 0.5f);

    public static float Coserp(float t) => 1f - Mathf.Cos(t * Mathf.PI * 0.5f);

    // ReSharper disable once UnusedMember.Global
    public static float Quadratic(float t) => t * t;

    public static float SmoothStep(float t) => t * t * (3f - 2f * t);

    // ReSharper disable once UnusedMember.Global
    public static float SmootherStep(float t) => t * t * t * (t * (6f * t - 15f) + 10f);
}
