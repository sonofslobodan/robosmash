﻿using System;
using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private SpawnerOrientation spawnerOrientation = SpawnerOrientation.None;
    [SerializeField] private Enemy enemyPrefab = null;
    [SerializeField] private Player player = null;
    [SerializeField] private int maxEnemies = 100;
    [SerializeField] private float enemySpawnRate = 2f;
    [SerializeField] private float maxSpawnHeight = 2f;

    public enum SpawnerOrientation
    {
        Left, 
        Right,
        Top,
        None
    }

    private readonly List<Enemy> enemyPool = new List<Enemy>();

    private float currentTime;

    private Vector3 RandomSpawnHeight =>
        transform.position + transform.up * Random.Range(0, maxSpawnHeight);

    private Quaternion SpawnRotation => Quaternion.LookRotation(transform.forward);

    public Player Player => player;

    private float enemySpawnRateValue;

    private float EnemySpawnRateValue
    {
        get
        {
            if (PlayerInfo.DifficultySettings == null)
            {
                return enemySpawnRate;
            }

            switch (spawnerOrientation)
            {
                case SpawnerOrientation.Left:
                    return PlayerInfo.DifficultySettings.LeftSpawnerSpawnRate;
                case SpawnerOrientation.Right:
                    return PlayerInfo.DifficultySettings.RightSpawnerSpawnRate;
                case SpawnerOrientation.Top:
                    return PlayerInfo.DifficultySettings.TopSpawnerSpawnRate;
                case SpawnerOrientation.None:
                    return enemySpawnRate;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    private void Awake()
    {
        Assert.IsNotNull(enemyPrefab);
        Assert.IsNotNull(player);
    }

    private void Update()
    {
        if (currentTime < EnemySpawnRateValue)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            currentTime = 0;
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        foreach (Enemy enemy in enemyPool)
        {
            if (enemy.gameObject.activeInHierarchy)
            {
                continue;
            }

            enemy.transform.SetPositionAndRotation(RandomSpawnHeight, SpawnRotation);
            enemy.TargetDirection = transform.forward;
            enemy.Initialize(this);
            enemy.gameObject.SetActive(true);

            return;
        }

        if (enemyPool.Count >= maxEnemies)
        {
            return;
        }

        Enemy spawnedEnemy = Instantiate(enemyPrefab, RandomSpawnHeight, SpawnRotation);
        spawnedEnemy.Initialize(this);
        spawnedEnemy.TargetDirection = transform.forward;
        enemyPool.Add(spawnedEnemy);
    }
}