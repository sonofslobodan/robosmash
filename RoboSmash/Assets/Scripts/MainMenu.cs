﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Text highScoreText = null;
    [SerializeField] private Text instructionsText = null;
    [SerializeField] private Button easyButton = null;
    [SerializeField] private Button normalButton = null;
    [SerializeField] private Button hardButton = null;
    [SerializeField] private DifficultySettings easyDifficultySettings = null;
    [SerializeField] private DifficultySettings normalDifficultySettings = null;
    [SerializeField] private DifficultySettings hardDifficultySettings = null;

    public const string MainMenuScene = "MainMenu";
    public const string ArenaScene = "Arena";

    private void Awake()
    {
        Assert.IsNotNull(highScoreText);
        Assert.IsNotNull(instructionsText);
        Assert.IsNotNull(easyButton);
        Assert.IsNotNull(normalButton);
        Assert.IsNotNull(hardButton);

        highScoreText.text = $"High Score: {PlayerInfo.HighScore}";
        instructionsText.text =
            $"Press {InputControls.MoveLeftKey} and {InputControls.MoveRightKey} to move. \n" +
            $" Press {InputControls.JumpKey} to jump. \n " +
            $"Click Mouse {InputControls.AttackMouseButton} to attack. \n " +
            $"Hold Mouse {InputControls.ShieldMouseButton} to shield. \n";

        easyButton.onClick.AddListener(StartEasyGame);
        normalButton.onClick.AddListener(StartNormalGame);
        hardButton.onClick.AddListener(StartHardGame);

        easyButton.onClick.AddListener(BeginLoadScene);
        normalButton.onClick.AddListener(BeginLoadScene);
        hardButton.onClick.AddListener(BeginLoadScene);
    }

    private void OnDestroy()
    {
        easyButton.onClick.RemoveListener(StartEasyGame);
        normalButton.onClick.RemoveListener(StartNormalGame);
        hardButton.onClick.RemoveListener(StartHardGame);

        easyButton.onClick.RemoveListener(BeginLoadScene);
        normalButton.onClick.RemoveListener(BeginLoadScene);
        hardButton.onClick.RemoveListener(BeginLoadScene);
    }

    private void StartEasyGame() => PlayerInfo.Initialize(easyDifficultySettings);

    private void StartNormalGame() => PlayerInfo.Initialize(normalDifficultySettings);

    private void StartHardGame() => PlayerInfo.Initialize(hardDifficultySettings);

    private static void BeginLoadScene() => SceneManager.LoadScene(ArenaScene);
}